package insta.downloader.base.main;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.logging.Logger;
import insta.downloader.base.classes.Downloader;

public class mainclass {

	static Logger _log = Logger.getGlobal();
	
	public static void main(String[] args) throws IOException {
		// Show Copyright and Info on Startup
		System.out.println("--------------------");
		System.out.println("Copyright: " + Constants.copyright);
		System.out.println("Version: " + Constants.version);
		System.out.println("--------------------");
		
		String inputUrl = "";
		
		// If no Argument passed
		if(args.length == 0) {
			_log.warning(Constants.msg_NoArgumentsGiven);
			return;
		} else {
			inputUrl = args[0];
		}
		
		// If also a specific path given
		if(args.length == 2) {
			// Check if Path Argument has Separator at the end
			if(!args[1].substring(args[1].length()-1).equals(File.separator)){
				args[1] += File.separator;
			}
			
//			// Check if Path Exists (If not -> Create)
//			File tmpFileDir = new File(Constants.downloadPath + args[1]);
//			if(!tmpFileDir.exists() || !tmpFileDir.isDirectory()) {
//				tmpFileDir.mkdirs();
//			}
			Constants.downloadPath = args[1];
		}
		
		// Download
		Downloader.simpleDownload(inputUrl);
	}

}
