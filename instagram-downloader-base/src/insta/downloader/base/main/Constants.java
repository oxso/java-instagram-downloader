package insta.downloader.base.main;

/**
 * Constants Class for Instagram Downloader
 * @author Mr. 0x50
 *
 */
public class Constants {
	
	public static final String version = "0.0.7 Dev";

	public static final String copyright = "(c) 2017 by Mr. 0x50";
	
	public static String downloadPath = "instagram-downloads/";
	
	// -- MESSAGES -- //
	public static final String msg_NoArgumentsGiven = "No Arguments given!";
}
