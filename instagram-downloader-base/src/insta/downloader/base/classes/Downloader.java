package insta.downloader.base.classes;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Logger;
import de.pascalrost.tools.Web;
import insta.downloader.base.main.Constants;

/**
 * Downloader Class for Instagram Downloader
 * @author Mr. 0x50
 *
 */
public class Downloader {
	
	private static Logger _log = Logger.getGlobal();
	
	/**
	 * Example Download Method
	 * Downloads Post Image or the last 12 Images of Public Profile
	 * @param inputUrl
	 * @throws IOException
	 */
	public static void simpleDownload(String inputUrl) throws IOException {
		if(HelperClass.isPostOrProfile(inputUrl) == PostOrProfile.Post) {
			// Download and Save Single Image Post
			downloadPost(inputUrl);
		} else {
			// Download an Save as most pictures as possible from Profile
			downloadProfilePictures(inputUrl);
		}
	}
	
	/**
	 * Download a posted Image (A single Image)
	 * @param postUrl URL of Post
	 * @throws IOException
	 */
	public static void downloadPost(String postUrl) throws IOException {
		String finalRawImageUrl = HelperClass.getRawImageUrlFromPost(postUrl);
		if(finalRawImageUrl == null) {
			return;
		}
		
		//String filename = Constants.downloadPath + Analyzer.analyzeFilename();
		String filename = Constants.downloadPath + HelperClass.generateNumbersFilename();
		try {
			filename = Constants.downloadPath + HelperClass.getFilenameForPost(postUrl);
		} catch(Exception e) {
			_log.warning("Could not get filename from URL.");
		}
		
		Web.downloadFile(finalRawImageUrl, filename);
		_log.info("Image saved as " + filename);
		_log.info("Done.");
	}
	
	/**
	 * Download as much Images as possible from Profile (mostly 12)
	 * @param profileUrl URL of Profile
	 * @throws IOException
	 */
	public static void downloadProfilePictures(String profileUrl) throws IOException {
		ArrayList<String> rawImageFileUrls = new ArrayList<String>();
		
		String srcOfProfile = HelperClass.getHTML(profileUrl);
		
		// Get all links after "display_src": "
		while(srcOfProfile.contains("\"display_src\": \"")) {
			String e = "";
			
			e = srcOfProfile.split("\"display_src\": \"")[1].split("\"")[0];
			srcOfProfile = srcOfProfile.split(e+"\"")[1];
			
			rawImageFileUrls.add(e);
		}
		
		// Download and Save all found Pictrues
		_log.info("Number of Images found: " + rawImageFileUrls.size());
		for(String rawImageFileUrl : rawImageFileUrls) {
			//String filename = Constants.downloadPath +  HelperClass.generateNumbersFilename();
			HelperClass.prepareDirectory(Constants.downloadPath + HelperClass.getDirectoryNameForProfileUrl(profileUrl));
			String filename = Constants.downloadPath + HelperClass.getDirectoryNameForProfileUrl(profileUrl) +  HelperClass.getFilenameForProfilePost(rawImageFileUrl);
			Web.downloadFile(rawImageFileUrl, filename);
			_log.info("Image saved as " + filename);
		}
		
		_log.info("Done.");
	}
	
}
