package insta.downloader.base.classes;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * HelperClass for Instagram Downloader
 * @author Mr. 0x50
 *
 */
public class HelperClass {
	
	static Logger _log = Logger.getGlobal();

	/**
	 * Analyze if URL is Post or Profile URL
	 * @param url URL
	 * @return Post or Profile ENUM
	 */
	public static PostOrProfile isPostOrProfile(String url) {
		if(url.contains("/p/")) {
			return PostOrProfile.Post;
		} else {
			return PostOrProfile.Profile;
		}
	}
	
	/**
	 * Generates an available numerous filename starting at 0.jpg
	 * @return available numerous filename (+.jpg)
	 */
	public static String generateNumbersFilename() {
		// Analyze Filename
		String filename = "";
		int i = 0;
		while(new File(i + ".jpg").exists()) {
			i++;
		}
		filename = i + ".jpg";
		
		return filename;
	}
	
	/**
	 * Gets the Filename from Instagram Post URL
	 * @param postUrl
	 * @return Filename (+.jpg)
	 */
	public static String getFilenameForPost(String postUrl) {
		return postUrl.split("/p/")[1].split("/")[0] + ".jpg";
	}
	
	/**
	 * Gets the Filename from Instagram ProfilePost URL
	 * @return Filename (+.jpg)
	 */
	public static String getFilenameForProfilePost(String displaySrcUrl) {
		return displaySrcUrl.substring(displaySrcUrl.lastIndexOf("/")+1);
	}
	
	/**
	 * Gets Directory Name for ProfilePost Images
	 * @param profileUrl
	 * @return Directory name for ProfilePosts
	 */
	public static String getDirectoryNameForProfileUrl(String profileUrl) {
		return profileUrl.split("https://www.instagram.com/")[1].substring(0, profileUrl.length() - 27) + File.separator;
	}
	
	/**
	 * Creates Directory if not exists
	 */
	public static void prepareDirectory(String path) {
		File fPath = new File(path);
		
		// Check Directory
		if(fPath.exists() && fPath.isDirectory() && fPath.canWrite()) {
			return;
		}
		
		// Create Directory
		if(!fPath.isDirectory()) {
			fPath.mkdirs();
			_log.info("Directory" + fPath.getAbsolutePath() + " created.");
		}
		
	}
	
	/**
	 * Get raw image url from post url
	 * @param url URL of Post
	 * @return URL of Raw Image File
	 * @throws IOException
	 */
	public static String getRawImageUrlFromPost(String url) {
		String finalImageUrl = "";
		String srcOfPage = "";
		try {
			srcOfPage = getHTML(url);	
		} catch(Exception e) {
			_log.warning("Raw Image File not found. Maybe this Post is private.");
			return null;
		}
		
		finalImageUrl = srcOfPage.split("\"display_url\": \"")[1].split("\"")[0];
		
		_log.info("Raw Image Url: " + finalImageUrl);
		return finalImageUrl;
	}
	
	/**
	 * Get HTML (as String) from a HTTP Request
	 * @param surl URL as String
	 * @return HTML Response as String
	 * @throws IOException
	 */
	public static String getHTML(String surl) throws IOException {
		
		// Connect to the URL using java's native library
	    URL url = new URL(surl);
	    HttpURLConnection request = (HttpURLConnection) url.openConnection();
	    
	    // Cookie
	    //request.setRequestProperty("Cookie", "user=demo123; domain=example.com; path=/autos");
	    
	    // Connect
	    request.connect();

	    // Get Response from Request Object
	    String response = new BufferedReader(new InputStreamReader((InputStream)request.getContent())).lines().collect(Collectors.joining("\n"));
	    
	    return response;
	}
}
